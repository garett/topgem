---This is not really a class, but a class's state.
---
---Objective: 1) accumule tasks, 2) get tasks executed, 3) popState()
---
---@class BootState: game.Game
local BootState = require 'game.state':addState('boot')

local love, coroutine = love, coroutine

---@type metatable
local counterMt = {
	__tostring = function(t) return t[#t]:gsub('\n', '') end,
	__call = function(t, dt, subject)
		t.elapsed, t.subject = t.elapsed + dt, subject
		t[#t + 1] = (subject .. ' (delta: ' .. dt .. ' seconds) (elapesed: ' .. t.elapsed .. ' seconds)' .. '\n')
		return t
	end,
}
BootState.path = (...)
BootState.next = {}
BootState.task = setmetatable({}, { __mode = 'k' })

function BootState:update(dt)
	if not coroutine.resume(self.co, dt) then
		local next = BootState.next[#BootState.next]
		if next then
			self:gotoState(next)
		else
			self:popState()
		end
	end
end

function BootState:draw()
	love.graphics.print(BootState.task[self])
end

function BootState:enteredState(...)
	BootState.task[self] = setmetatable({}, counterMt)
	BootState.task[self].elapsed = 0

	local window, input = love.window, require 'game.input'

	---@todo tasking or something better
	self.co = coroutine.create(function(dt)
		BootState.task[self](dt, 'Entering booting state...')
		print(BootState.task[self])
		coroutine.yield()
		local ready = love.graphics.isActive() and 'updateMode' or 'setMode'
		local ww, wh, flags = window.getMode()
		local dw, dh = window.getDesktopDimensions()
		flags.x, flags.y = (dw / 2) - (ww / 2), (dh / 2) - (wh / 2)
		window[ready](ww, wh, flags)
		BootState.task[self](dt, 'Window setup done.')
		print(BootState.task[self])
		coroutine.yield()
		-- for _ = 1, 3 do
		-- 	love.timer.sleep(1)
		-- 	BootState.task[self](1, 'Sleeping...')
		-- 	print(BootState.task[self])
		-- 	coroutine.yield()
		-- end
		-- BootState.task[self](dt, 'Sleep done.')
		-- print(BootState.task[self])
		-- coroutine.yield()
		BootState.task[self](dt, 'Boot done.')
	end)
end

function BootState:exitedState(...)
	BootState.task[self].elapsed = 0
	print('Exiting booting state...')
end

return BootState
