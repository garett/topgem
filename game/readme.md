Each file on this directory overwrites module under "module/game" directory. 
But first, the `package.path` must be something like this:

```
?.lua;?/init.lua;module/?.lua;module/?/init.lua
```
