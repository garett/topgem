w.i.p.
======

setup the project by:

* initialize project's required submodules
	* `git submodule update --init`

run the game by:

* get love2d installed
	* get it here: https://love2d.org

* execute: `love .`
	* the `.` is referring to current directory: this directory

[link to repo](https://codeberg.org/garett/topgem) 

[online build](https://garett.codeberg.page/topgem)

__Disclosure:__
this git repo *also* works as my personal training drill: either learn how all possible available features "safety" and/or keeping myself sharp log past activities. reading docs is not enough.
