---@class moduleLoader: table
local M = {
	FS_SEP = '/',
	MOD_SEP = '.',
	LUA_PATH = '?.lua;?/init.lua',

	--
	--stores default configuration as members of module too.
	--

	--Path to this module (dot as directory separator)
	path = (...),

	--Module to be loaded (if conf.preload is not passed)
	preload = {
		-- 'middleclass',
		-- 'stateful',
		-- 'ww',
		-- 'game',
	},

	--Load all module of passed path?
	loadAll = true,
}

local patternescape, removetrails, pathify

--Escape escapable pattern escape.
---@param str string
---@return string notrail
---@return integer count
patternescape = function(str)
	return str:gsub("[%(%)%.%%%+%-%*%?%[%]%^%$]", "%%%1")
end

---@param str string
---@return string notrail
---@return integer count
removetrails = function(str)
	return str:gsub('[%s%.\\/]+$', ''):gsub('%.init$', '')
end

---@param path string
---@param offset? integer
---@return boolean isRegistered
M.registerPath = function(path, offset)
	local filesystem, normalizedPath, pathSep, passedPath, dotMod, dotModInit, indexToBeInserted
	local package, table, love = package, table, love
	if path == '' then return true end

	-- get OS specified slash seperator character
	pathSep = package.config:sub(1, 1)
	normalizedPath = path:gsub('[\\/]', pathSep) .. pathSep
	-- prep templates; this simply subtitute "?" with normalized path plus "?"
	passedPath = M.LUA_PATH:gsub('[\\/]', pathSep):gsub('?', normalizedPath .. '?')

	if package.path:find(passedPath, 1, true) then
		return true
	end

	--This does not append  the path string at the end, but in-middle after certain pattern.
	local pathTable, dotModPattern, dotModInitPattern = {}, '^%.[\\/]%?%.lua$', '^%.[\\/]%?[\\/]init%.lua$'

	for str in package.path:gmatch("([^;]+)") do
		table.insert(pathTable, str)
		-- Look up for: ./?.lua
		if not dotMod and str:match(dotModPattern) then
			dotMod = #pathTable
		end
		-- Look up for: ./?/init.lua
		if not dotModInit and str:match(dotModInitPattern) then
			dotModInit = #pathTable
		end
	end

	indexToBeInserted = (dotModInit or dotMod or 1) + 1
	indexToBeInserted = indexToBeInserted - (offset or 0)
	table.insert(pathTable, indexToBeInserted, passedPath)
	package.path = table.concat(pathTable, ';')

	if love and love.filesystem then
		local requirePath, lovePath
		filesystem = love.filesystem
		requirePath = filesystem.getRequirePath()
		lovePath = passedPath:gsub('[\\/]', M.FS_SEP) -- enforce slash
		if requirePath:find(lovePath, 1, true) then
			return true
		end
		---Love2D simply append the string, simple.
		filesystem.setRequirePath(requirePath .. ';' .. lovePath)
	end

	return false
end

---Returns the passed module path string (dot separator) to defined filesystem path string (enforce slash "/").
---@param path string
---@param modSep? string
---@param fsSep? string
---@return string fsPath
---@return integer count
function M.toFS(path, modSep, fsSep)
	modSep, fsSep = modSep or M.MOD_SEP, fsSep or M.FS_SEP
	return path:gsub(patternescape(modSep), fsSep)
end

---Accent a path. This equal to dirname(). Called by number of passed `n`.
---@param str string
---@param n? integer
---@return string
---@return integer count
function M.accent(str, n, sep)
	local pattern = ('(.*' .. patternescape(sep) .. ')(.*)')
	for _ = 1, (n or 1) do
		str = removetrails(str)
			:gsub(pattern, '%1')
	end
	return removetrails(str)
end

---Load the module (same as require) and also alias it onto package.loaded.
---@param modName string
---@return unknown
function M.require(modName)
	local package_loaded = package.loaded
	local modData = package_loaded[modName]
		or require(modName .. '.' .. modName)

	if package_loaded[modName] == nil then
		package_loaded[modName] = modData
	end

	return modData
end

---@param fsPath string
---@param basePath? string
function M.requireAll(fsPath, basePath, fsSep, modSep)
	local filesystem = love.filesystem
	fsPath = fsPath or basePath and M.toFS(basePath)
	fsSep, modSep = fsSep or M.FS_SEP, modSep or M.MOD_SEP
	for _, module in ipairs(filesystem.getDirectoryItems(fsPath)) do
		local entryPointPath = fsPath .. fsSep .. module .. fsSep .. module .. '.lua'
		if basePath then
			module = removetrails(basePath) .. modSep .. module
		end
		if filesystem.getInfo(entryPointPath, 'file') then
			M.require(module)
		end
	end
end

---Register passed path to LUA_PATH and Love2D require path.
---@param conf? { path: string?, preload: string[]?, loadAll: boolean|string? }
---@return moduleLoader
function M.init(conf)
	-- Nothing to do.
	if conf == nil then return M end

	local ipairs, type = ipairs, type

	conf = type(conf) == 'string' and { path = conf } or conf or {}
	conf.path = removetrails(conf.path or M.path)
	conf.preload = conf.preload or M.preload
	conf.loadAll = conf.loadAll or M.loadAll

	conf.path = M.toFS(conf.path)
	M.registerPath(conf.path)

	for _, module in ipairs(conf.preload) do
		M.require(module)
	end

	if conf.loadAll then
		M.requireAll(conf.loadAll == true and conf.path or conf.loadAll --[[ @as string ]])
	end

	return M
end

return setmetatable(M, { __call = function(self, conf) return self.init(conf) end })
