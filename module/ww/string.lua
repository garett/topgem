local tonumber, tostring, pcall, require, type =
	tonumber, tostring, pcall, require, type

---@class ww.string: stringlib
local string = setmetatable({}, { __index = string })

local DIR = (...):match("(.-)[^%.]+$")
local hasTable, tablelib = pcall(require, DIR .. 'math')
local hasHelper, helper = pcall(require, DIR .. 'helper')
local _table_array, _memoize, _getSep, _getTrim, _getLimitFn, _patternescape

---
---Constant are stored in stringlib table for reasons.
---

string.ESCAPED_PATTERN = "[%(%)%.%%%+%-%*%?%[%]%^%$]"
string.SPLIT_SEP_PATTERN = "([%S]+)"
string.WHITESPACE_PATTERN = "^[%s]*(.-)[%s]*$"
string.SPACE_PATTERN = "(%S+)(%s*)"
string.WORDWRAP_LIMIT = 72

---The table library used by this module.
---@type ww.table|tablelib
tablelib = hasTable and tablelib or table
local tinsert, tconcat, ticlear = tablelib.insert, tablelib.concat, tablelib.iclear
	or function(t) for i = #t, 1, -1 do t[i] = nil end end

---
---Based on rxi's lume (and bit of unpublished changes by me).
---https://github.com/rxi/lume
---

---@param ... any
---@return table
_table_array = tablelib.array or function(...)
	local t = {}
	for x in ... do t[#t + 1] = x end
	return t
end

---@generic T: function
---@param fn T
---@return T
_memoize = hasHelper and helper.memoize or function(fn)
	return fn
end

---Escape string pattern.
---@param str string
---@return string
---@return integer
function _patternescape(str)
	return str:gsub(string.ESCAPED_PATTERN, "%%%1")
end

string.patternescape = _patternescape

---@overload fun(sep: string, raw?: boolean): string
_getSep = _memoize(function(sep, raw)
	local str = raw and sep or string.patternescape(sep)
	return ("(.-)(" .. str .. ")")
end)

---@overload fun(chars: string, raw?: boolean): string
_getTrim = _memoize(function(chars, raw)
	local str = raw and chars or string.patternescape(chars)
	return ("^[" .. str .. "]*(.-)[" .. str .. "]*$")
end)

---@overload fun(limit: number): fun(length:number, t: table): boolean, table
_getLimitFn = _memoize(function(limit)
	return function(length, t) return #length >= limit, t end
end)

---Returns an array of the words in the string `str`. If `sep` is provided it is used as the delimiter, consecutive delimiters are not grouped together and will delimit empty strings.
---@param str string
---@param sep? string
---@return string[]
function string.split(str, sep)
	if not sep then
		return _table_array(str:gmatch(string.SPLIT_SEP_PATTERN))
	end
	return sep == '' and { str }
		or _table_array((str .. sep):gmatch(_getSep(sep)))
end

---Trims the whitespace from the start and end of the string `str` and returns the new string. If a `chars` value is set the characters in `chars` are trimmed instead of whitespace.
---@param str string
---@param chars string
---@return any ...
function string.trim(str, chars)
	if not chars then return str:match(string.WHITESPACE_PATTERN) end
	chars = string.patternescape(chars)
	return str:match(_getTrim(chars))
end

---Returns `str` wrapped to `limit` number of characters per line, by default limit is `72`. `limit` can also be a function which when passed a string, returns `true` if it is too long for a single line.
---
---This function wraps words by characters' width. Use `love.Font` for graphics pixel by font size instead.
---@param str string
---@param limit? integer|fun(t: table): boolean
---@param raw? boolean|table # Returns as table of string by line? This works similar to `love.Font:getWrap`.
---@return string|table result # Wrapped text string OR table of string lines.
function string.wordwrap(str, limit, raw)
	limit = limit or string.WORDWRAP_LIMIT
	local check, rtn
	check = type(limit) == 'number'
		and _getLimitFn(limit)
		or limit --[[ @as fun(length:number, t: table): boolean, table ]]
	rtn = type(raw) == 'table' and rtn or {}
	local space, newline, line, length = ' ', '\n', {}, 0
	for word, spaces in str:gmatch(string.SPACE_PATTERN) do
		length = length + #word
		if check(length, line) then
			tinsert(rtn, tconcat(line, space))
			ticlear(line)
			length = #word
		end
		tinsert(line, word)
		for c in spaces:gmatch(".") do
			length = length + #c
			if c == newline then
				tinsert(rtn, tconcat(line, space))
				ticlear(line)
				length = #word
			end
			tinsert(line, c)
		end
	end
	return raw and rtn or tconcat(rtn, newline)
end

---Formerly `lume.format`.
---@param str string
---@param vars string
---@return string
function string.format2(str, vars)
	if not vars then return str end
	local f = function(x)
		return tostring(vars[x] or vars[tonumber(x)] or "{" .. x .. "}")
	end
	return (str:gsub("{(.-)}", f))
end

---Ensure ww.string inherit all table's functions. Uselful for future pairs iteration.
for k, v in pairs(getmetatable(string).__index) do
	if rawget(string, k) == nil then
		string[k] = v
	end
end

return string
