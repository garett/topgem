local require, setmetatable, getmetatable, pcall, next, select, assert =
	require, setmetatable, getmetatable, pcall, next, select, assert

---@class ww.table: tablelib
local table = setmetatable({}, { __index = table })

local DIR = (...):match("(.-)[^%.]+$")
local hasNew, modNew = pcall(require, 'table.new')
local hasClear, modClear = pcall(require, 'table.clear')
local hasMath, mathlib = pcall(require, DIR .. 'math')
local hasHelper, helper = pcall(require, DIR .. 'helper')
local _get_comp, _get_iteratee, _absindex, _memoize, _is_string, _is_table, _is_func, _is_callable

---The math library used by this module.
---@type ww.math|mathlib
mathlib = hasMath and mathlib or math

if hasHelper then
	---@cast helper ww.helper
	_is_string = helper.string
	_is_table = helper.table
	_is_callable = helper.callable
	_is_func = helper.func
	_memoize = helper.memoize
else
	local type, _string, _table, _func = type, 'string', 'table', 'function'

	_is_string = function(x) return type(x) == _string end
	_is_table = function(x) return type(x) == _table end
	_is_func = function(x) return type(x) == _func end
	_is_callable = function(x)
		return _is_func(x) or getmetatable(x) and _is_func(getmetatable(x).__call)
	end

	---@generic T: function
	---@param fn T
	---@return T
	_memoize = function(fn) return fn end
end

---@overload fun(key: string): fun(t1: table, t2: table): boolean
_get_comp = _memoize(function(key)
	return function(a, b) return a[key] < b[key] end
end)

---@overload fun(x: any): fun(z: any): any
_get_iteratee = _memoize(function(x)
	if _is_callable(x) then return x end
	if _is_table(x) then
		return function(z)
			for k, v in next, x do
				if z[k] ~= v then return false end
			end
			return true
		end
	end
	return function(z) return z[x] end
end)

---@param len integer
---@param i integer
---@return integer
_absindex = function(len, i)
	return i < 0 and (len + i + 1) or i
end


---The internal random function. Used on most extended function.
---This field designed to be replaced by other (better) random function.
---@protected
table.random = love and love.math.random or mathlib.random

---
---Polyfilling...?
---

table.unpack = _G.unpack or table.unpack

table.new = hasNew and modNew or function()
	return {} -- returns normal one
end

---@param t table
---@return table t
table.clear = hasClear and modClear or function(t)
	for k in next, t do
		t[k] = nil
	end
	-- original table.clear removes metatable too
	setmetatable(t, nil)
	return t
end

---Same as table.clear but for array.
---@param t table
---@return table t
function table.iclear(t)
	for i = 1, #t do
		t[i] = nil
	end
	return t
end

---
---Based on edubart's tabler
---https://github.com/edubart/nelua-lang/blob/master/lualib/nelua/utils/tabler.lua
---

---Copy a table into another, in-place (mutate).
---@param t1 table
---@param t2 table
function table.update(t1, t2)
	for k, v in next, t2 do
		t1[k] = v
	end
	return t1
end

---Shallow copy for table.
---@param t table
---@return table rtn
function table.copy(t)
	local rtn = table.new(#t, 0)
	for k, v in next, t do
		rtn[k] = v
	end
	return rtn
end

---Shallow copy for array.
---@param t table
---@return table rtn
function table.icopy(t)
	local rtn = table.new(#t, 0)
	for i = 1, #t do
		rtn[i] = t[i]
	end
	return rtn
end

---Shallow copy a table and update its elements.
---@param source table
---@param update table
---@return table rtn
function table.updatecopy(source, update)
	return table.update(table.copy(source), update)
end

---Make table `destination` identical to table `source` (including metatables).
---@generic T: table
---@param destination T
---@param source table
---@return T
function table.mirror(destination, source)
	if rawequal(destination, source) then
		return destination
	end
	setmetatable(destination, nil)
	table.clear(destination)
	table.update(destination, source)
	return setmetatable(destination, getmetatable(source))
end

---
---Meta magics.
---

---Inherit table's metatable values. This does not overwrite.
---@generic T: table
---@param t T
---@param recursively? boolean
---@return T
function table.inherit(t, recursively)
	---@type metatable?
	local mt = getmetatable(t)
	if mt and mt.__index then
		local rawget = rawget
		for k, v in next, mt.__index do
			if rawget(t, k) == nil then
				t[k] = v
			end
		end
		if recursively then
			---@type metatable?
			mt = getmetatable(mt)
			while mt and mt.__index do
				for k, v in next, mt.__index do
					if rawget(t, k) == nil then
						t[k] = v
					end
				end
				mt = getmetatable(mt)
			end
		end
	end
	return t
end

---Unpack function generator.
---@todo
---@overload fun(length: integer): fun(t): ...: unknown
table._unpack = _memoize(function(length)

end)

---Faster static unpack. Returns 4 values since passed index `i`. `i` is 0 by default. Best usage: color, vector (up to 4).
---@generic T
---@param t T[]
---@param i? integer
---@return T   ...
---@nodiscard
function table.unpack4(t, i)
	i = i or 1
	return t[i], t[i + 1], t[i + 2], t[i + 3]
end

---Similar to `unpack4` but 6. Best usage: 2d transformation metrics, `love.handlers` arguments length.
---@generic T
---@param t T[]
---@param i? integer
---@return T   ...
---@nodiscard
function table.unpack6(t, i)
	i = i or 1
	return t[i], t[i + 1], t[i + 2],
		t[i + 3], t[i + 4], t[i + 5]
end

---Similar to `unpack4` but 8.
---@generic T
---@param t T[]
---@param i? integer
---@return T   ...
---@nodiscard
function table.unpack8(t, i)
	i = i or 1
	return t[i], t[i + 1], t[i + 2], t[i + 3],
		t[i + 4], t[i + 5], t[i + 6], t[i + 7]
end

---@generic T
---@param t T[]
---@param i? integer
---@return T   ...
---@nodiscard
function table.unpack10(t, i)
	i = i or 1
	return t[i], t[i + 1], t[i + 2], t[i + 3], t[i + 4],
		t[i + 5], t[i + 6], t[i + 7], t[i + 8], t[i + 9]
end

---@generic T
---@param t T[]
---@param i? integer
---@return T   ...
---@nodiscard
function table.unpack12(t, i)
	i = i or 1
	return t[i], t[i + 1], t[i + 2], t[i + 3],
		t[i + 4], t[i + 5], t[i + 6], t[i + 7],
		t[i + 8], t[i + 9], t[i + 10], t[i + 11]
end

---
---Based on airstruck's knife.base, but without `constructor`.
---
---@generic T: table
---@param source T
---@param subtype? T
---@param meta? metatable
---@return T T
function table.extend(source, subtype, meta)
	subtype = subtype or {}
	meta = meta or { __index = source }
	return setmetatable(subtype, {
		__index = source,
		__call = function(self, ...) return setmetatable({}, meta) end,
	})
end

---
---Based on rxi's lume (and bit of unpublished changes by me).
---https://github.com/rxi/lume
---

---Pushes all the given values to the end of the table `t` and returns the pushed values. Nil values are ignored.
---@param t table
---@param ... any
---@return any
function table.push(t, ...)
	local argc = select('#', ...)
	for i = 1, argc do
		t[#t + 1] = select(i, ...)
	end
	return ...
end

---Removes the first instance of the value `x` if it exists in the table `t`.
---@param t table
---@param x any
function table.removal(t, x)
	for k, v in next, t do
		if v == x then
			t[k] = nil
			break
		end
	end
	return x
end

---Similar to `push`, but replaces from index 1. Passed Nil value is skipped YET same index.
---
---		local t = { 1, 2, 3, 4, 5, 6 }
---		table.assign(t, 9, 8, nil, 6) -- `t` becomes { 9, 8, 3, 6, 5, 6 }
---
---@param t table
---@param ... any
---@return any
function table.assign(t, ...)
	local argc = select('#', ...)
	for i = 1, argc do
		local x = select(i, ...)
		if x ~= nil then
			t[i] = x
		end
	end
	return ...
end

---Formerly `table.extend`. Copies all the fields from the source tables to the table t and returns t. If a key exists in multiple tables the right-most table's value is used.
---@param t table
---@param ... table
---@return table
function table.replace(t, ...)
	local argc = select('#', ...)
	for i = 1, argc do
		local x = select(i, ...)
		if x then
			for k, v in next, x do
				t[k] = v
			end
		end
	end
	return t
end

---Returns a shuffled copy of the array t.
---@param t table
---@return table
function table.shuffle(t)
	local rand, rtn = table.random, table.new(#t, 0)
	for i = 1, #t do
		local j = rand(i, #t)
		t[i], t[j] = t[j], t[i]
	end
	return rtn
end

---Formerly `table.sort`. Returns a copy of the array `t` with all its items sorted. If comp is a function it will be used to compare the items when sorting. If comp is a string it will be used as the key to sort the items by.
function table.sort2(t, comp)
	local sort, rtn = table.sort, table.copy(t)
	if comp then
		if _is_string(comp) then
			sort(rtn, _get_comp(comp))
		else
			sort(rtn, comp)
		end
	else
		sort(rtn)
	end
	return rtn
end

---Iterates the supplied iterator and returns an array filled with the values.
---@param ... unknown
---@return table
function table.array(...)
	local t = {}
	for x in ... do
		t[#t + 1] = x
	end
	return t
end

---Iterates the table `t` and calls the function `fn` on each value followed by the supplied additional arguments; if `fn` is a string the method of that name is called for each value. The function returns `t` unmodified.
---@generic T: table
---@param t T
---@param fn string|fun(v: any, ...: unknown)
---@param ... unknown
---@return T
function table.each(t, fn, ...)
	if _is_string(fn) then
		---@cast fn string
		for _, v in next, t do v[fn](v, ...) end
	else
		---@cast fn function
		for _, v in next, t do fn(v, ...) end
	end
	return t
end

---Applies the function `fn` to each value in table `t` and returns a new table with the resulting values.
---@param t table
---@param fn fun(z: any): any
---@return table
function table.map(t, fn)
	fn = _get_iteratee(fn)
	local rtn = {}
	for k, v in next, t do
		rtn[k] = fn(v)
	end
	return rtn
end

---Returns true if all the values in `t` table are true. If a `fn` function is supplied it is called on each value, true is returned if all of the calls to `fn` return true.
---@param t table
---@param fn? fun(z: any): boolean
---@return boolean
function table.all(t, fn)
	fn = _get_iteratee(fn)
	for _, v in next, t do
		if not fn(v) then return false end
	end
	return true
end

---Returns true if any of the values in `t` table are true. If a `fn` function is supplied it is called on each value, true is returned if any of the calls to `fn` return true.
---@param t table
---@param fn? fun(z: any): boolean
---@return boolean
function table.any(t, fn)
	fn = _get_iteratee(fn)
	for _, v in next, t do
		if fn(v) then return true end
	end
	return false
end

---Applies `fn` on two arguments cumulative to the items of the array `t`, from left to right, so as to reduce the array to a single value. If a `first` value is specified the accumulator is initialised to this, otherwise the first value in the array is used. If the array is empty and no `first` value is specified an error is raised.
---@param t table
---@param fn fun(z: any, v: any): any
---@param first? any
---@return boolean
function table.reduce(t, fn, first)
	local started, acc
	started = first ~= nil
	acc = first
	for _, v in next, t do
		if started then
			acc = fn(acc, v)
		else
			acc, started = v, true
		end
	end
	assert(started, "reduce of an empty table with no first value")
	return acc
end

---Returns a copy of the `t` array with all the duplicate values removed.
---@param t table
---@return table
function table.unique(t)
	local rtn = {}
	for k in next, table.invert(t) do
		rtn[#rtn + 1] = k
	end
	return rtn
end

---Calls `fn` on each value of `t` table. Returns a new table with only the values where `fn` returned true. If `retainkeys` is true the table is not treated as an array and retains its original keys.
---@param t table
---@param fn fun(v: any): boolean
---@param retainkeys? boolean
---@return table
function table.filter(t, fn, retainkeys)
	fn = _get_iteratee(fn)
	local rtn = {}
	if retainkeys then
		for k, v in next, t do
			if fn(v) then rtn[k] = v end
		end
	else
		for _, v in next, t do
			if fn(v) then rtn[#rtn + 1] = v end
		end
	end
	return rtn
end

---The opposite of `filter()`: Calls fn on each value of `t` table; returns a new table with only the values where `fn` returned false. If `retainkeys` is true the table is not treated as an array and retains its original keys.
---@param t table
---@param fn fun(v: any): boolean
---@param retainkeys? boolean
---@return table
function table.reject(t, fn, retainkeys)
	fn = _get_iteratee(fn)
	local rtn = {}
	if retainkeys then
		for k, v in next, t do
			if not fn(v) then rtn[k] = v end
		end
	else
		for _, v in next, t do
			if not fn(v) then rtn[#rtn + 1] = v end
		end
	end
	return rtn
end

---Returns a new table with all the given tables merged together. If a key exists in multiple tables the right-most table's value is used.
---@param ... table
---@return table
function table.merge(...)
	local rtn, argc = {}, select('#', ...)
	for i = 1, argc do
		local t = select(i, ...)
		for k, v in next, t do
			rtn[k] = v
		end
	end
	return rtn
end

---Formerly `lume.concat`. Returns a new array consisting of all the given arrays concatenated into one.
---@param ... table
---@return table
function table.flat(...)
	local rtn, argc = {}, select('#', ...)
	for i = 1, argc do
		local t = select(i, ...)
		if t ~= nil then
			for _, v in next, t do
				rtn[#rtn + 1] = v
			end
		end
	end
	return rtn
end

---Returns the index/key of value in t. Returns nil if that value does not exist in the table.
---@param t table
---@param value any
---@return any
function table.find(t, value)
	for k, v in next, t do
		if v == value then return k end
	end
	return nil
end

---Returns the value and key of the value in table `t` which returns true when `fn` is called on it. Returns nil if no such value exists.
---@param t table
---@param fn fun(x: any): boolean
---@return any
---@return any
function table.match(t, fn)
	fn = _get_iteratee(fn)
	for k, v in next, t do
		if fn(v) then return v, k end
	end
	return nil
end

---Counts the number of values in the table `t`. If a `fn` function is supplied it is called on each value, the number of times it returns true is counted.
---@param t table
---@param fn? fun(x: any): boolean
---@return integer
function table.count(t, fn)
	local count = 0
	if fn then
		fn = _get_iteratee(fn)
		for _, v in next, t do
			if fn(v) then count = count + 1 end
		end
	else
		if t[1] ~= nil then
			return #t
		end
		for _ in next, t do count = count + 1 end
	end
	return count
end

---Mimics the behaviour of Lua's `string.sub`, but operates on an array rather than a string. Creates and returns a new array of the given slice.
---@param t table
---@param i? integer
---@param j? integer
---@return table
function table.slice(t, i, j)
	i = i and _absindex(#t, i) or 1
	j = j and _absindex(#t, j) or #t
	local rtn = {}
	for x = i < 1 and 1 or i, j > #t and #t or j do
		rtn[#rtn + 1] = t[x]
	end
	return rtn
end

---Returns the first element of an array or nil if the array is empty. If `n` is specificed an array of the first `n` elements is returned.
---@param t table
---@param n? integer
---@return any
function table.first(t, n)
	if not n then return t[1] end
	return table.slice(t, 1, n)
end

---Returns the last element of an array or nil if the array is empty. If `n` is specificed an array of the last `n` elements is returned.
---@param t table
---@param n? integer
---@return any
function table.last(t, n)
	if not n then return t[#t] end
	return table.slice(t, -n, -1)
end

---Returns a copy of the table where the keys have become the values and the values the keys.
---@param t table
---@return table
function table.invert(t)
	local rtn = {}
	for k, v in next, t do rtn[v] = k end
	return rtn
end

---Returns a copy of the table filtered to only contain values for the given keys.
---@param t table
---@param ... any
---@return table
function table.pick(t, ...)
	local rtn, argc = {}, select("#", ...)
	for i = 1, argc do
		local k = select(i, ...)
		rtn[k] = t[k]
	end
	return rtn
end

---Returns an array containing each key of the table.
---@param t any
---@return table
function table.keys(t)
	local rtn = {}
	for k in next, t do rtn[#rtn + 1] = k end
	return rtn
end

---Ensure ww.table inherit all table's functions. Uselful for future pairs iteration.
table:inherit()

return table
