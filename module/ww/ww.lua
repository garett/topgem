---Wonky Wrapper library for LOVE2D and Lua
---@class WW: love
---@field helper ww.helper
---@field math ww.math
---@field physics ww.physics
---@field string ww.string
---@field table ww.table
local WW = setmetatable({}, { __index = love })

local DIR = (...):match("(.-)[^%.]+$")
local require = require

for _, module in ipairs({
	'helper',
	'math',
	'physics',
	'string',
	'table',
}) do
	WW[module] = require(DIR .. module)
end


---
---Types (classes) and its constructor.
---

---@module 'class.world2'
WW.World2 = require(DIR .. 'world2')

---@param ... unknown
---@return World2 instance
WW.newWorld = function(...)
	return WW.World2:new(...)
end

return WW
