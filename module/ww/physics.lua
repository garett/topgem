local love, setmetatable, ipairs, require = love, setmetatable, ipairs, require
local love_physics, graphics = love.physics, love.graphics
local weakKeys = { __mode = 'k' }
---
---Some stuff scrapped from: http://love2d.org/wiki/Tutorial:PhysicsDrawing
---
---@class ww.physics: love.physics
local physics = setmetatable({}, { __index = love_physics })

---@type { join: love.LineJoin, style: love.LineStyle, width: number }
physics.DrawLine = {
	join = graphics.getLineJoin(),
	style = graphics.getLineStyle(),
	width = graphics.getLineWidth(),
}

---@type love.DrawMode
physics.DrawMode = 'line'

---@type table<love.World|love.Joint|love.Body|love.Fixture|love.Shape, love.DrawMode>
physics._cachedDrawMode = setmetatable({}, weakKeys)

---@param obj love.World|love.Joint|love.Body|love.Fixture|love.Shape
---@param mode? love.DrawMode
function physics.setDrawMode(obj, mode)
	physics._cachedDrawMode[obj] = mode
end

---@param obj love.World|love.Joint|love.Body|love.Fixture|love.Shape
---@return love.DrawMode? mode
function physics.getDrawMode(obj)
	return physics._cachedDrawMode[obj]
end

---@type number[]
physics.Color = {}

---@type table<love.World|love.Joint|love.Body|love.Fixture|love.Shape, table[]>
physics._cachedColor = setmetatable({}, weakKeys)

function physics.setColor(obj, ...)

end

function physics.getColor(obj)

end

---@protected
---@type table<love.ShapeType, string>
physics._ShapeTypeFn = {
	polygon = 'drawPolygonShape',
	circle = 'drawCircleShape',
	chain = 'drawChainShape',
	edge = 'drawEdgeShape',
}

---@param shape love.PolygonShape
---@param mode? love.DrawMode
---@vararg number # The vertices of the polygon.
function physics.drawPolygonShape(shape, mode, ...)
	mode = mode or physics.getDrawMode(shape)
	graphics.polygon(mode or physics.DrawMode, ...)
end

---@param shape love.CircleShape
---@param mode? love.DrawMode
---@vararg number # By order: x, y, radius.
function physics.drawCircleShape(shape, mode, ...)
	mode = mode or physics.getDrawMode(shape)
	graphics.circle(mode or physics.DrawMode, ...)
end

---@param shape love.PolygonShape
---@param mode? love.DrawMode
---@vararg number # By order: x1, y1, x2, x2.
function physics.drawChainShape(shape, mode, ...)
	graphics.line(...)
end

---@param shape love.PolygonShape
---@param mode? love.DrawMode
---@vararg number # By order: x1, y1, x2, x2.
function physics.drawEdgeShape(shape, mode, ...)
	graphics.line(...)
end

---@param shape love.Shape
---@param mode? love.DrawMode
---@param fixture love.Fixture
---@param body? love.Body
function physics.drawShape(shape, mode, fixture, body)
	---Future proof: Shape and mode are passed to the function, even the function doesn't need these.
	---@type fun(shape: love.Shape, mode?: love.DrawMode, ...: number)?
	local shapeFn = physics[physics._ShapeTypeFn[shape:getType()]]

	---Skip if it does not exist. Do not throw error.
	if not shapeFn then return end

	body = body or fixture:getBody()

	if shape:typeOf("CircleShape") then
		---@cast shape love.CircleShape
		---@diagnostic disable-next-line: missing-parameter
		local cx, cy = body:getWorldPoints(shape:getPoint())
		return shapeFn(shape, mode, cx, cy, shape:getRadius())
	end

	---@cast shape love.PolygonShape|love.ChainShape|love.EdgeShape
	return shapeFn(shape, mode, body:getWorldPoints(shape:getPoints()))
end

---@param fixture love.Fixture
---@param mode? love.DrawMode
function physics.drawFixture(fixture, mode)
	return physics.drawShape(fixture:getShape(), mode, fixture)
end

---Draw a body. It still drawn even if it is off the screen.
---@param body love.Body
---@param mode? love.DrawMode
---@param listOfFixture? love.Fixture[]
function physics.drawBody(body, mode, listOfFixture)
	mode = mode or physics.getDrawMode(body)
	listOfFixture = listOfFixture or body:getFixtures()
	for _, fixture in ipairs(listOfFixture) do
		physics.drawFixture(fixture, mode)
	end
end

---@param joint love.Joint
---@param mode? love.DrawMode
---@param listOfBodies? love.Body[]
function physics.drawJoint(joint, mode, listOfBodies)
	mode = mode or physics.getDrawMode(joint)
	listOfBodies = listOfBodies or joint:getBodies()
	for _, body in ipairs(listOfBodies) do
		physics.drawBody(body, mode)
	end
end

---@param world love.World
---@param mode? love.DrawMode
---@param listOfBodies love.Body[]
function physics.drawWorldBodies(world, mode, listOfBodies)
	mode = mode or physics.getDrawMode(world)
	listOfBodies = listOfBodies or world:getBodies()
	for i = 1, #listOfBodies do
		physics.drawBody(listOfBodies[i], mode)
	end
end

---@param world love.World
---@param listOfJoints love.Joint[]
function physics.drawWorldJoints(world, listOfJoints)
	listOfJoints = listOfJoints or world:getJoints()
end

return physics
