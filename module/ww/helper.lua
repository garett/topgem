local type, select, unpack, setmetatable = type, select, unpack, setmetatable
local weakKeys = { __mode = 'k' }

---@class ww.helper
local helper = {}

local __string, __table, __function, __number = 'string', 'table', 'function', 'number'

function helper.noop(...) end --luacheck: ignore

---@param x any
---@param ... type
---@return boolean
function helper.either(x, ...)
	local argc = select('#', ...)
	for i = 1, argc do
		if type(x) == select(i, ...) then
			return true
		end
	end
	return false
end

---@param x any
---@return boolean
function helper.string(x) return type(x) == __string end

---@param x any
---@return boolean
function helper.number(x) return type(x) == __number end

---@param x any
---@return boolean
function helper.table(x) return type(x) == __table end

---@param x any
---@return boolean
function helper.func(x) return type(x) == __function end

---@param x any
---@return boolean
function helper.bool(x) return x == true or x == false end

---@param x any
---@return boolean
function helper.anil(x) return x == nil end

---@param x any
---@return boolean
function helper.nan(x) return x ~= x end

---@param x any
---@return boolean
function helper.callable(x)
	local _is_fn, mt = helper.func, getmetatable(x)
	if _is_fn(x) then return true end
	return mt and _is_fn(mt.__call)
end

local memoize_nil, memoize_fnkey = {}, setmetatable({}, weakKeys)

---Returns a wrapper function to fn where the results for any given set of arguments are cached. `memoize()` is useful when used on functions with slow-running computations. Unlike original `lume.memoize()`, cache tables are weak table.
---@generic T: function
---@param fn T
---@return T
function helper.memoize(fn)
	local cache = setmetatable({}, weakKeys)
	return function(...)
		for i = 1, select("#", ...) do
			local a = select(i, ...) or memoize_nil
			cache[a] = cache[a] or {}
			cache = cache[a]
		end
		cache[memoize_fnkey] = cache[memoize_fnkey] or { fn(...) }
		return unpack(cache[memoize_fnkey])
	end
end

return helper
