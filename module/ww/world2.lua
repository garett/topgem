local class, physics, World2Class, World2ClassStatic, World2
local setmetatable, getmetatable, require, pairs =
	setmetatable, getmetatable, require, pairs
local weakKeys, DIR = { __mode = 'k' }, (...):match("(.-)[^%.]+$")

---@module 'middleclass'
---@type middleclass
class = require 'middleclass'

---@module 'ww.physics'
physics = require(DIR .. 'physics')

---This class is based on love.World.
---@class World2Class: class, World2
---@overload fun(...): instance: World2
---@field new fun(self: self, xg?: number, yg?: number, sleep?: boolean): instance: World2
---@field static self
World2Class = class('World2')

---@class World2Class
World2ClassStatic = World2Class.static

---Contains initiated instance tied by `love.World`. Works like userdata for worlds, kinda.
---@type table<love.World, World2>
World2ClassStatic.instanceList = setmetatable({}, weakKeys)
---@type table<love.ShapeType|"rectangle", fun(...): love.Shape>
World2ClassStatic.shapeList = {}

function World2ClassStatic:initialize(overwrite)
	---NOTE: `self` in this function is the class!
	local _physics, _dummyWorld, _shapeList, _memo_node, _memo_nil, _memoize
	local rawget, select, unpack = rawget, select, unpack or table.unpack

	---@type love.physics
	_physics = self.physics or physics
	self.physics = not overwrite and rawget(self, 'physics') or _physics
	_dummyWorld = _physics.newWorld()
	for method in pairs(getmetatable(_dummyWorld).__index) do
		if not method:match('^__') then
			---@param klass World2
			self[method] = not overwrite and rawget(self, method) or function(klass, ...)
				return klass._World[method](...)
			end
		end
	end
	_dummyWorld:release()

	_shapeList = self.static.shapeList
	_shapeList.chain = _physics.newChainShape
	_shapeList.circle = function(x, y, radius) return _physics.newCircleShape(x, y, radius) end
	_shapeList.edge = function(x1, y1, x2, y2) return _physics.newEdgeShape(x1, y1, x2, y2) end
	_shapeList.polygon = _physics.newPolygonShape
	_shapeList.rectangle = function(x, y, width, height) return _physics.newRectangleShape(x, y, width, height) end

	_memo_node, _memo_nil = setmetatable({}, weakKeys), {}
	_memoize = function(fn)
		local cache = setmetatable({}, weakKeys)
		return function(...)
			local c = cache
			for i = 1, select("#", ...) do
				local a = select(i, ...) or _memo_nil
				c[a] = c[a] or setmetatable({}, weakKeys)
				c = c[a]
			end
			c[_memo_node] = c[_memo_node] or { fn(...) }
			return unpack(c[_memo_node])
		end
	end
	for key, value in pairs(_shapeList) do
		_shapeList[key] = _memoize(value)
	end

	return self
end

---@class World2: instance, love.World
---@field class fun(self: self): World2Class
World2 = World2Class

World2.physics = setmetatable({}, { __index = physics })

function World2:initialize(...)
	self:setWorld(World2ClassStatic.physics.newWorld(...))
end

---@return love.World internalWorld
function World2:getWorld()
	return self._World
end

---@param internalWorld love.World
---@return self
function World2:setWorld(internalWorld)
	self._World = internalWorld
	return self
end

---@param x? number # The x position of the body.
---@param y? number # The y position of the body.
---@param type? love.BodyType # The type of the body.
---@return love.Body body # A new body.
function World2:newBody(x, y, type)
	return self.physics.newBody(self._World, x, y, type)
end

---@param body love.Body # The body which gets the fixture attached.
---@param shape love.Shape # The shape to be copied to the fixture.
---@param density? number # The density of the fixture.
---@return love.Fixture fixture # The new fixture.
function World2:newFixture(body, shape, density)
	return self.physics.newFixture(body, shape, density)
end

---@overload fun(self: self, type: 'chain', loop: boolean, x1: number, y1: number, x2: number, y2: number, ...: number): shape: love.ChainShape
---@overload fun(self: self, type: 'circle', x: number, y: number, radius: number): shape: love.CircleShape
---@overload fun(self: self, type: 'edge', x1: number, y1: number, x2: number, y2: number): shape: love.EdgeShape
---@overload fun(self: self, type: 'polygon', x1: number, y1: number, x2: number, y2: number, x3: number, y3: number, ...: number): shape: love.PolygonShape
---@overload fun(self: self, type: 'rectangle', x: number, y: number, width: number, height: number, angle?: number): shape: love.PolygonShape
---@param type love.ShapeType|"rectangle"
---@param ... unknown
---@return love.Shape shape
function World2:newShape(type, ...)
	local klass = self:class()
	local shape = assert(klass.static.shapeList[type], 'Undefined shape. Got ' .. tostring(type))
	return shape(...)
end

function World2:draw()
end

---
---Below functions are taken from love 12 (dev). Translated into Lua code, humanly.
---

---@param type love.BodyType
---@param x number
---@param y number
---@param radius number
---@return love.Body
---@return love.Fixture
function World2:newCircleBody(type, x, y, radius)
	local shape, body, fixture
	---@type love.CircleShape
	shape = self:newShape('circle', 0, 0, radius)
	body = self:newBody(x, y, type)
	fixture = self:newFixture(body, shape)
	return body, fixture
end

function World2:newRectangleBody(type, x, y, w, h, angle)
	local shape, body, fixture
	---@type love.PolygonShape
	shape = self:newShape('rectangle', 0, 0, w, h, angle)
	body = self:newBody(x, y, type)
	fixture = self:newFixture(body, shape)
	return body, fixture
end

function World2:newPolygonBody(type, ...)
	local shape, body, fixture, argc, arg1
	---@type love.PolygonShape
	shape = self:newShape('polygon', ...)
	body = self:newBody(...)
	return body
end

function World2:newEdgeBody(type, x1, y1, x2, y2)
	local shape, body, fixture, wx, wy
	wx, wy = (x2 - x1) / 2, (y2 - y1) / 2
	---@type love.EdgeShape
	shape = self:newShape('edge', x1 - wx, y1 - wy, x2 - wx, y2 - wy)
	body = self:newBody(wx, wy, type)
	fixture = self:newFixture(body, shape)
	return body, fixture
end

function World2:newChainBody(type, ...)
	local shape, body, fixture
	body = self:newBody(...)
	return body
end


World2Class:initialize()
return World2Class
