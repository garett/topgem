---@class ww.math: mathlib
local math = setmetatable({}, { __index = math })

local math_floor, math_ceil, math_abs, math_atan2, math_sqrt, math_cos, math_sin =
	math.floor, math.ceil, math.abs, math.atan2, math.sqrt, math.cos, math.sin

---Local random function utilizes the better available one.
local random = love and love.math.random or math.random

---
---Based on rxi's lume (and bit of unpublished changes by me).
---https://github.com/rxi/lume
---

---Returns the number x clamped between the numbers min and max
---@param x number
---@param min number
---@param max number
---@return number
function math.clamp(x, min, max)
	return x < min and min or (x > max and max or x)
end

---Rounds `x` to the nearest integer; rounds away from zero if we're midway between two integers. If `increment` is set then the number is rounded to the nearest increment.
---@param x number
---@param increment? number
---@return number
function math.round(x, increment)
	if increment then return math.round(x / increment) * increment end
	return x >= 0 and math_floor(x + .5) or math_ceil(x - .5)
end

---Returns `1` if `x` is 0 or above, returns `-1` when `x` is negative.
---@param x number
---@return integer
function math.sign(x)
	return x < 0 and -1 or 1
end

---Returns the linearly interpolated number between `a` and `b`, `amount` should be in the range of 0 - 1; if `amount` is outside of this range it is clamped.
---@param a number
---@param b number
---@param amount number
---@return number
function math.lerp(a, b, amount)
	return a + (b - a) * math.clamp(amount, 0, 1)
end

---@param a number
---@param b number
---@param amount number
---@return number
function math.smooth(a, b, amount)
	local t, m
	t = math.clamp(amount, 0, 1)
	m = t * t * (3 - 2 * t)
	return a + (b - a) * m
end

---Ping-pongs the number `x` between 0 and 1.
---@param x number
---@return number
function math.pingpong(x)
	return 1 - math_abs(1 - x % 2)
end

---Returns the distance between the two points. If `squared` is true then the squared distance is returned -- this is faster to calculate and can still be used when comparing distances.
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@param squared boolean
---@return number
function math.distance(x1, y1, x2, y2, squared)
	local dx, dy, s
	dx = x1 - x2
	dy = y1 - y2
	s = dx * dx + dy * dy
	return squared and s or math_sqrt(s)
end

---Returns the angle between the two points.
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@return number
function math.angle(x1, y1, x2, y2)
	return math_atan2(y2 - y1, x2 - x1)
end

---Returns the rotated of a point around the origin by an angle
---@param x number
---@param y number
---@param r number
---@return number
---@return number
function math.rotate(x, y, r)
	local c, s
	s = math_sin(r)
	c = math_cos(r)
	return c * x - s * y, s * x + c * y
end

--Returns the length of a vector from the origin
---@param x number
---@param y number
---@return number
function math.length(x, y)
	return math_sqrt(x * x + y * y)
end

---Given an `angle` and `magnitude`, returns a vector.
---@param angle number
---@param magnitude number
---@return number
---@return number
function math.vector(angle, magnitude)
	return math_cos(angle) * magnitude, math_sin(angle) * magnitude
end

---Returns a random value from array t. If the array is empty an error is raised.
---@param t any[]
---@return any
function math.randomchoice(t)
	return t[random(#t)]
end

---Takes the argument table `t` where the keys are the possible choices and the value is the choice's weight. A weight should be 0 or above, the larger the number the higher the probability of that choice being picked. If the table is empty, a weight is below zero or all the weights are 0 then an error is raised.
---@generic T: any
---@param t { [T]: number }
---@return T?
function math.weightedchoice(t)
	local next, assert, sum = next, assert, 0
	for _, v in next, t do
		assert(v >= 0, "weight value less than zero")
		sum = sum + v
	end
	assert(sum ~= 0, "all weights are zero")
	local rnd = random(sum)
	for k, v in next, t do
		if rnd < v then return k end
		rnd = rnd - v
	end
end

---
---Stuff that normally exists on any programming language's math library
---

---Similar as `math.clamp()`, but wrap around `t` table's index length.
---@param i integer
---@param t table
function math.wrap(i, t)
	return math_floor(math.clamp(i, 1, #t))
end

function math.clamp01(x)
	return math.clamp(x, 0, 1)
end

---Ensure ww.math inherit all table's functions. Uselful for future pairs iteration.
for k, v in pairs(getmetatable(math).__index) do
	if rawget(math, k) == nil then
		math[k] = v
	end
end

return math
