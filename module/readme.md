# Module directory

## Module loading
DO NOT directly load any module from this directory (expect "init.lua" on this directory).
"init.lua" will registed path to this directory onto LUA_PATH (and Love2D require path).

This is incorrect:
```lua
local middleclass = require 'module.middleclass.middleclass'
```

This is correct:
```lua
local middleclass = require 'middleclass.middleclass'
```

## Module aliasing

"init.lua" has `require` function that automatically solve the repetive module name path.

Instead this:
```lua
local middleclass = require 'middleclass.middleclass'
```

Do this this instead:
```lua
local middleclass = require 'middleclass'
```

The module requires to be preloaded by module's `require` at first. This function modify `package.loaded`.
```lua
local module = require 'module' -- module/init.lua

---What this do:
-- 1. load by `require('middleclass.middleclass')`
-- 2. alias by reference `package.loaded['middleclass.middleclass']` onto `package.loaded['middleclass']` (if `nil`)
local middleclass = module.load('middleclass')
```
These two are expected to refering the same module:
```lua
local m1 = require 'middleclass.middleclass'
local m2 = require 'middleclass'

assert(m1 == m2)
```

## Module naming

1. A module and its sub-module files are contained in a directory.
2. A module's directory MUST be the same name as module's entry point.

