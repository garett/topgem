---The module entry point, maybe.
local M = {
	_VERSION = '0.0.0',
	_LICENSE = 'CC0 1.0 Universal',
}

local require, modulePath = require, ((...):match("(.-)[^%.]+$"))

---Manually require the game
local simplegametutorials = {
	['asteroids'] = require(modulePath .. 'asteroids'),
	['bird'] = require(modulePath .. 'bird'),
	['blackjack'] = require(modulePath .. 'blackjack'),
	['blocks'] = require(modulePath .. 'blocks'),
	['eyes'] = require(modulePath .. 'eyes'),
	['fifteen'] = require(modulePath .. 'fifteen'),
	['flowers'] = require(modulePath .. 'flowers'),
	['life'] = require(modulePath .. 'life'),
	['repeat'] = require(modulePath .. 'repeat'),
	['snake'] = require(modulePath .. 'snake'),
	['sokoban'] = require(modulePath .. 'sokoban'),
}

local love, gameState, bootState = love, require 'game.state', require 'game.state.boot'

---@class PlaygroundState: game.Game
M.state = gameState:addState('playground')
table.insert(bootState.next, 'playground')

M.state.subStates = {}
for name, module in pairs(simplegametutorials) do
	local stateName = 'playground.' .. name
	M.state.subStates[stateName] = gameState:addState(stateName, module)
end

function M.state:update()
	self.input()
	if self.input:pressed 'back' then
		self:popState()
	end
end

function M.state:draw()
	love.graphics.print('This is playground.')
end

return M
