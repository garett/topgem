local Input, InputClass, InputClassStatic
local require, error, tonumber, ipairs, pairs = require, error, tonumber, ipairs, pairs

---@type 'middleclass'
local class, ww = require 'middleclass', require 'ww'
local math_sqrt, math_abs, table_copy = ww.math.sqrt, ww.math.abs, ww.table.copy

---Based on [tesselode/baton](https://github.com/tesselode/baton)
---@class game.InputClass: class, game.Input
---@overload fun(conf?: game.Input.conf): instance: game.Input
---@field new fun(self: self, conf?: game.Input.conf): instance: game.Input
InputClass = class('game.Input')

---@class game.InputClass
---@field parseSource fun(self: self, str: string): str1: string, str2: string # splits a source definition into type and value
---@field parseAxis fun(self: self, str: string): str1: string, str2: string # splits an axis value into axis and direction
---@field parseHat fun(self: self, str: string): str1: string, str2: string # splits a joystick hat value into hat number and direction
InputClassStatic = InputClass.static

---@type love.keyboard
InputClassStatic.keyboard = love.keyboard or {
	isDown = function() return 0 end,
	isScancodeDown = function() return 0 end,
}

---@type love.mouse
InputClassStatic.mouse = love.mouse or {
	isDown = function() return 0 end,
}

---@class game.Input.conf
InputClassStatic.conf = {
	---@type table<love.KeyConstant, string[]>
	controls = {
		left = { 'key:left', 'key:a', 'axis:leftx-', 'button:dpleft' },
		right = { 'key:right', 'key:d', 'axis:leftx+', 'button:dpright' },
		up = { 'key:up', 'key:w', 'axis:lefty-', 'button:dpup' },
		down = { 'key:down', 'key:s', 'axis:lefty+', 'button:dpdown' },
		action = { 'key:x', 'button:a' },
		back = { 'key:escape', 'button:b' },
	},
	---@type table<string, love.KeyConstant[]>
	pairs = {
		move = { 'left', 'right', 'up', 'down' }
	},
	---@type love.Joystick?
	joystick = love.joystick.getJoysticks()[1],
	---@type number
	deadzone = 0.5,
	---@type boolean
	squareDeadzone = false,
}

InputClassStatic.patterns = {
	parseSource = '(.+):(.+)',
	parseAxis = '(.+)([%+%-])',
	parseHat = '(%d)(.+)',
}

for method in pairs(InputClassStatic.patterns) do
	InputClassStatic[method] = function(self, source)
		return source:match(self.patterns[method])
	end
end

InputClassStatic.keyboardMouse = {
	-- checks whether a keyboard key is down or not
	key = function(key)
		return InputClass.keyboard.isDown(key) and 1 or 0
	end,

	-- checks whether a keyboard key is down or not,
	-- but it takes a scancode as an input
	sc = function(sc)
		return InputClass.keyboard.isScancodeDown(sc) and 1 or 0
	end,

	-- checks whether a mouse buttons is down or not.
	-- note that baton doesn't detect mouse movement, just the buttons
	mouse = function(button)
		---@diagnostic disable-next-line: param-type-mismatch
		return InputClass.mouse.isDown(tonumber(button)) and 1 or 0
	end,
}

InputClassStatic.joystick = {
	-- checks the position of a joystick axis
	axis = function(joystick, value)
		local axis, direction = InputClass:parseAxis(value)
		-- "a and b or c" is ok here because b will never be boolean
		value = tonumber(axis) and joystick:getAxis(tonumber(axis))
			or joystick:getGamepadAxis(axis)
		if direction == '-' then value = -value end
		return value > 0 and value or 0
	end,

	-- checks whether a joystick button is held down or not
	-- can take a number or a GamepadButton string
	button = function(joystick, button)
		-- i'm intentionally not using the "a and b or c" idiom here
		-- because joystick.isDown returns a boolean
		if tonumber(button) then
			return joystick:isDown(tonumber(button)) and 1 or 0
		else
			return joystick:isGamepadDown(button) and 1 or 0
		end
	end,

	-- checks the direction of a joystick hat
	hat = function(joystick, value)
		local hat, direction = InputClass:parseHat(value)
		return joystick:getHat(hat) == direction and 1 or 0
	end,
}


---@class game.Input: instance
---@overload fun() # Equal to `instance:update()`.
---@field class fun(self: self): class: game.InputClass
---@field conf game.Input.conf
Input = InputClass

function Input:initialize(conf)
	self
		:_loadConf(conf)
		:_initControls(conf)
		:_initPairs(conf)
	self._activeDevice = 'none'
end

---@return game.Input
function Input:_loadConf(conf)
	local klass_conf, copy = self:class().conf, table_copy
	conf = conf or {}
	conf.controls = conf.controls or copy(klass_conf.controls)
	conf.pairs = conf.pairs or copy(klass_conf.pairs)
	conf.deadzone = conf.deadzone or klass_conf.deadzone
	conf.squareDeadzone = conf.squareDeadzone or klass_conf.squareDeadzone
	conf.joystick = conf.joystick or klass_conf.joystick
	---@type game.Input.conf
	self.conf = conf
	return self
end

---@param conf game.Input.conf
function Input:_initControls(conf)
	conf, self._controls = conf or self.conf, {}
	for controlName, sources in pairs(conf.controls) do
		self._controls[controlName] = {
			sources = sources,
			rawValue = 0,
			value = 0,
			down = false,
			downPrevious = false,
			pressed = false,
			released = false,
		}
	end
	return self
end

---@param conf game.Input.conf
function Input:_initPairs(conf)
	conf, self._pairs = conf or self.conf, {}
	for pairName, controls in pairs(conf.pairs) do
		self._pairs[pairName] = {
			controls = controls,
			rawX = 0,
			rawY = 0,
			x = 0,
			y = 0,
			down = false,
			downPrevious = false,
			pressed = false,
			released = false,
		}
	end
	return self
end

function Input:_setActiveDevice()
	local conf, klass = self.conf, self:class()
	if self._activeDevice == 'joy' and not conf.joystick then
		self._activeDevice = 'none'
	end
	for _, control in pairs(self._controls) do
		for _, source in ipairs(control.sources) do
			local type, value = klass:parseSource(source)
			if klass.keyboardMouse[type] then
				if klass.keyboardMouse[type](value) > conf.deadzone then
					self._activeDevice = 'kbm'
					return
				end
			elseif conf.joystick and klass.joystick[type] then
				if klass.joystick[type](conf.joystick, value) > conf.deadzone then
					self._activeDevice = 'joy'
				end
			end
		end
	end
end

function Input:_getControlRawValue(control)
	local rawValue, conf, klass = 0, self.conf, self:class()
	for _, source in ipairs(control.sources) do
		local type, value = klass:parseSource(source)
		if klass.keyboardMouse[type] and self._activeDevice == 'kbm' then
			if klass.keyboardMouse[type](value) == 1 then
				return 1
			end
		elseif klass.joystick[type] and self._activeDevice == 'joy' then
			rawValue = rawValue + klass.joystick[type](conf.joystick, value)
			if rawValue >= 1 then
				return 1
			end
		end
	end
	return rawValue
end

function Input:_updateControls()
	local conf, _controls = self.conf, self._controls
	for _, control in pairs(_controls) do
		control.rawValue = self:_getControlRawValue(control)
		control.value = control.rawValue >= conf.deadzone and control.rawValue or 0
		control.downPrevious = control.down
		control.down = control.value > 0
		control.pressed = control.down and not control.downPrevious
		control.released = control.downPrevious and not control.down
	end
end

function Input:_updatePairs()
	local conf, _pairs, _controls = self.conf, self._pairs, self._controls
	for _, pair in pairs(_pairs) do
		-- get raw x and y
		local l = _controls[pair.controls[1]].rawValue
		local r = _controls[pair.controls[2]].rawValue
		local u = _controls[pair.controls[3]].rawValue
		local d = _controls[pair.controls[4]].rawValue
		pair.rawX, pair.rawY = r - l, d - u

		-- limit to 1
		local len = math_sqrt(pair.rawX ^ 2 + pair.rawY ^ 2)
		if len > 1 then
			pair.rawX, pair.rawY = pair.rawX / len, pair.rawY / len
		end

		-- deadzone
		if conf.squareDeadzone then
			pair.x = math_abs(pair.rawX) > conf.deadzone and pair.rawX or 0
			pair.y = math_abs(pair.rawY) > conf.deadzone and pair.rawY or 0
		else
			pair.x = len > conf.deadzone and pair.rawX or 0
			pair.y = len > conf.deadzone and pair.rawY or 0
		end

		-- down/pressed/released
		pair.downPrevious = pair.down
		pair.down = pair.x ~= 0 or pair.y ~= 0
		pair.pressed = pair.down and not pair.downPrevious
		pair.released = pair.downPrevious and not pair.down
	end
end

-- checks for changes in inputs
function Input:update()
	self:_setActiveDevice()
	self:_updateControls()
	self:_updatePairs()
end

-- gets the value of a control or axis pair without deadzone applied
function Input:getRaw(name)
	local _pairs, _controls = self._pairs, self._controls
	if _pairs[name] then
		return _pairs[name].rawX, _pairs[name].rawY
	elseif _controls[name] then
		return _controls[name].rawValue
	else
		error('No control with name "' .. name .. '" defined', 3)
	end
end

-- gets the value of a control or axis pair with deadzone applied
function Input:get(name)
	local _pairs, _controls = self._pairs, self._controls
	if _pairs[name] then
		return _pairs[name].x, _pairs[name].y
	elseif _controls[name] then
		return _controls[name].value
	else
		error('No control with name "' .. name .. '" defined', 3)
	end
end

-- gets whether a control or axis pair is "held down"
function Input:down(name)
	local _pairs, _controls = self._pairs, self._controls
	if _pairs[name] then
		return _pairs[name].down
	elseif _controls[name] then
		return _controls[name].down
	else
		error('No control with name "' .. name .. '" defined', 3)
	end
end

-- gets whether a control or axis pair was pressed this frame
function Input:pressed(name)
	local _pairs, _controls = self._pairs, self._controls
	if _pairs[name] then
		return _pairs[name].pressed
	elseif _controls[name] then
		return _controls[name].pressed
	else
		error('No control with name "' .. name .. '" defined', 3)
	end
end

-- gets whether a control or axis pair was released this frame
function Input:released(name)
	local _pairs, _controls = self._pairs, self._controls
	if _pairs[name] then
		return _pairs[name].released
	elseif _controls[name] then
		return _controls[name].released
	else
		error('No control with name "' .. name .. '" defined', 3)
	end
end

--[[
	gets the currently active device (either "kbm", "joy", or "none").
	this is useful for displaying instructional text. you may have
	a menu that says "press ENTER to confirm" or "press A to confirm"
	depending on whether the player is using their keyboard or gamepad.
	this function allows you to detect which they used most recently.
]]
function Input:getActiveDevice()
	return self._activeDevice
end

function Input:getJoystick()
	return self.conf.joystick
end

function Input:__call() return self:update() end

return InputClass
