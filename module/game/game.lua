---The collective module of game.
---Don't be confused with GameState object, see `game.Game`.
---@class game
---@field newGame fun(): gameInstance: game.Game
---@field newEvent fun(): gameInstance: game.Event
local M = {
	path = (...),
}

---
---Arguments to `love.event.push()` and the like. This  inherit `love.Event`.
---
---@alias game.EventName
---| "focus" # Window focus gained or lost
---| "joystickpressed" # Joystick pressed
---| "joystickreleased" # Joystick released
---| "keypressed" # Key pressed
---| "keyreleased" # Key released
---| "mousepressed" # Mouse pressed
---| "mousereleased" # Mouse released
---| "quit" # Quit
---| "resize" # Window size changed by the user
---| "visible" # Window is minimized or un-minimized by the user
---| "mousefocus" # Window mouse focus gained or lost
---| "threaderror" # A Lua error has occurred in a thread
---| "joystickadded" # Joystick connected
---| "joystickremoved" # Joystick disconnected
---| "joystickaxis" # Joystick axis motion
---| "joystickhat" # Joystick hat pressed
---| "gamepadpressed" # Joystick's virtual gamepad button pressed
---| "gamepadreleased" # Joystick's virtual gamepad button released
---| "gamepadaxis" # Joystick's virtual gamepad axis moved
---| "textinput" # User entered text
---| "mousemoved" # Mouse position changed
---| "lowmemory" # Running out of memory on mobile devices system
---| "textedited" # Candidate text for an IME changed
---| "wheelmoved" # Mouse wheel moved
---| "touchpressed" # Touch screen touched
---| "touchreleased" # Touch screen stop touching
---| "touchmoved" # Touch press moved inside touch screen
---| "directorydropped" # Directory is dragged and dropped onto the window
---| "filedropped" # File is dragged and dropped onto the window.
---
---| "post_focus"
---| "post_joystickpressed"
---| "post_joystickreleased"
---| "post_keypressed"
---| "post_keyreleased"
---| "post_mousepressed"
---| "post_mousereleased"
---| "post_quit"
---| "post_resize"
---| "post_visible"
---| "post_mousefocus"
---| "post_threaderror"
---| "post_joystickadded"
---| "post_joystickremoved"
---| "post_joystickaxis"
---| "post_joystickhat"
---| "post_gamepadpressed"
---| "post_gamepadreleased"
---| "post_gamepadaxis"
---| "post_textinput"
---| "post_mousemoved"
---| "post_lowmemory"
---| "post_textedited"
---| "post_wheelmoved"
---| "post_touchpressed"
---| "post_touchreleased"
---| "post_touchmoved"
---| "post_directorydropped"
---| "post_filedropped"
---
---| "load" # Called once on every game (re)start.
---| "update" # Update on every frame.
---| "draw" # Draw on every frame.
---| "setup" # `love.load` get defined, called ONCE on program start up.
---
---| "post_load"
---| "post_update"
---| "post_draw"
---| "post_setup"


local require, pairs, setmetatable, weakKeys, modulePath =
	require, pairs, setmetatable, { __mod = 'k' }, (M.path:match("(.-)[^%.]+$"))

M._hashCached = setmetatable({}, weakKeys)
M.class = {
	---@module 'game.event'
	Event = require(modulePath .. 'event'),
	---@module 'game.state'
	---@type game.GameClass
	Game = require(modulePath .. 'state'),
}

for className in pairs(M.class) do
	local constructorName = 'new' .. className
	M[constructorName] = M[constructorName] or function(...)
		return M.class[className]:new(...)
	end
end

function M:setup(conf)
	if not conf or conf and conf.skipsetup then return end
	local love = love
	for className, classModule in pairs(M.class) do
		local hasSetup = classModule.setup --[[@as function? ]]
		if hasSetup then
			love.dispact('setup', classModule, M, className)
			hasSetup(classModule, M, className)
			love.dispact('post_setup', classModule, M, className)
		end
	end
end

function M:hook(event, t, overwrite)
	local helper, postEvent = require 'ww'.helper, ('post_' .. event)
	if not overwrite then
		local original = helper.callable(t[event])
			and t[event] or helper.noop --[[@as function fun(...) ]]
		t[event] = function(...)
			original(...)
			love.dispact(event, ...)
			love.dispact(postEvent, ...)
			return love.returns[event]
		end
	else
		t[event] = function(...)
			love.dispact(event, ...)
			love.dispact(postEvent, ...)
			return love.returns[event]
		end
	end
	return self
end

---Patch things up.
function M:init(love, conf)
	---@class love
	---@field handlers table<love.Event, fun(...: any): ...: any>
	love = love or _G.love
	conf = conf or {}
	if M._hashCached[love] then return end

	local Event = self.class.Event --[[@as game.EventClass ]]

	---Hook dispatcher to love's callbacks. This overwrite; old ones are not kept!
	---
	---See: https://github.com/love2d/love/blob/11.5/src/modules/love/callbacks.lua
	---
	---This patch made all callback event that call functions under love uncallable.
	---Example: Defined `love.keypressed` does nothing, but `love.handlers.keypressed`.
	---
	love.dispact = Event:new()

	---@param event game.EventName
	---@param ... unknown
	---@return game.Event.Handler
	love.on = function(event, ...)
		return love.dispact:on(event, ...)
	end

	love.returns = love.returns or {}

	for event in pairs(love.handlers) do
		self:hook(event, love.handlers, conf.overwrite)
	end

	for _, event in pairs({ 'update', 'draw' }) do
		self:hook(event, love, conf.overwrite)
	end

	self._hashCached[love] = true
end

---Configurate game load here.
---@param conf table
---@return love.load
function M:load(conf)
	self:init(conf.engine, conf)
	self:setup(conf)

	---@type game.Game?
	local gameInstance

	return function(...)
		love.dispact('load', gameInstance, ...)
		if gameInstance then gameInstance:release() end
		gameInstance = self.class.Game:new(conf.boot or 'boot')
		gameInstance:load(...)
		love.dispact('post_load', gameInstance, ...)
	end
end

return setmetatable(M, { __call = M.load, __tostring = function(t) return t.path end })
