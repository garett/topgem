local class, stateful, ww, Game, GameClass, GameClassStatic
local require, pairs, print, select = require, pairs, print, select

---@module 'middleclass'
---@type middleclass
class = require 'middleclass'

---@module 'stateful'
---@type stateful
stateful = require 'stateful'

---@module 'ww'
ww = require 'ww'

---@class game.GameClass: class, game.Game
---@field new fun(self: self, ...): instance: game.Game
---@field addState fun(stateName: string, super?: table): state: game.Game
GameClass = class('game.Game'):include(stateful)

---@class game.GameClass
GameClassStatic = GameClass.static

GameClassStatic.STATES_PATH = {
	'game/state/'
}
GameClassStatic.TIMED_OUT = 15
GameClassStatic.NO_GAME_MESSAGE = [[No game state.
Press "escape" to quit or wait for timed out (15 seconds).
]]

GameClassStatic._EventListener = function(...)
	local love = love
	return love.dispact
end

function GameClassStatic:setEventListener(f)
	GameClassStatic._EventListener = f
end

function GameClassStatic:getEventListener(...)
	return GameClassStatic._EventListener(...)
end

function GameClassStatic:setup(...)
	local filesystem = love.filesystem
	for _, path in pairs(self.STATES_PATH) do
		for _, stateModule in ipairs(filesystem.getDirectoryItems(path)) do
			local modpath = path:gsub('[\\/]', '.') .. (stateModule:gsub('%.lua$', ''))
			require(modpath)
		end
	end
end

---@class game.Game: instance, stateful
Game = GameClass

function Game:initialize(state)
	---@type table<game.Event.Handler,boolean>
	self.handlers = {}
	self:on('update')
	self:on('draw')

	self.input = require('game.input'):new()

	if state then self:pushState(state) end
end

function Game:load(...)
	local table = table
	print(table.concat(select(1, ...), '\t'))
end

function Game:update(dt)
	local love = love
	self.input:update()
	if not self.elapse then
		self.elapse = 0
		print(GameClass.NO_GAME_MESSAGE)
		print('Timed out on next ' .. GameClass.TIMED_OUT .. ' seconds.')
	else
		self.elapse = self.elapse + dt
		if self.elapse > GameClass.TIMED_OUT or self.input:pressed 'back' then
			print('Quiting...')
			love.event.quit()
		end
	end
end

function Game:draw()
	local love = love
	love.graphics.print(GameClass.NO_GAME_MESSAGE)
end

function Game:release()
	for handler in pairs(self.handlers) do
		handler:remove()
		self.handlers[handler] = nil
	end
end

---@param self game.Game
---@param action string
---@param ... string
---@return game.Game
local pause_resume_dry = function(self, action, ...)
	local callback, argt, argc
	callback = select(1, ...)
	if ww.helper.callable(callback) then
		---@cast callback fun(self: game.Game, ...): boolean
		for handler in pairs(self.handlers) do
			if callback(self, handler, ...) then
				handler[action](handler)
			end
		end
	else
		argc = select('#', ...)
		if argc > 0 then
			argt = {}
			for i = 1, argc do
				argt[select(i, ...)] = true
			end
			for handler in pairs(self.handlers) do
				if argt[handler.name] then
					handler[action](handler)
				end
			end
		else
			for handler in pairs(self.handlers) do
				handler[action](handler)
			end
		end
	end
	return self
end

---@overload fun(self: self, callback: fun(self: self): boolean, ...): self
---@param ... string
---@return game.Game
function Game:pause(...)
	return pause_resume_dry(self, 'remove', ...)
end

---@overload fun(self: self, callback: fun(self: self): boolean, ...): self
function Game:resume(...)
	return pause_resume_dry(self, 'register', ...)
end

---@param event game.EventName
---@param ... unknown
---@return game.Event.Handler
function Game:on(event, ...)
	local eventListener = GameClass:getEventListener()
	local handler = eventListener:on(self, event, ...)
	self.handlers[handler] = true
	return handler
end

return Game
