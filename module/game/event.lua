local Event, EventClass, EventClassStatic, EventHandler, EventHandlerClass, EventHandlerClassStatic
local select, type, error, require, setmetatable, assert = select, type, error, require, setmetatable, assert
local weakKeys = { __mode = 'k' }

---@type 'middleclass'
local class = require 'middleclass'

---
---Based on [airstruck's knife.event](https://github.com/airstruck/knife/).
---
---@class game.EventClass: class, game.Event
---@overload fun(...): instance: game.Event
---@field new fun(self: self, ...): instance: game.Event
EventClass = class('game.Event')

---@class game.EventClass
EventClassStatic = EventClass.static

---@class game.Event.HandlerClass: class, game.Event.Handler
---@overload fun(event: game.Event, eventName: string, callback: function, subject: any): instance: game.Event.Handler
---@field new fun(self: self, event: game.Event, eventName: string, callback: function, subject: any): instance: game.Event.Handler
EventHandlerClass = class('game.Event.Handler')

EventClassStatic.Handler = EventHandlerClass

---@class game.Event.Handler
EventHandlerClassStatic = EventHandlerClass.static

---@type table<game.Event.Handler, game.Event>
EventHandlerClassStatic._EventRegistry = setmetatable({}, weakKeys)

---@type table<game.Event.Handler, boolean>
EventHandlerClassStatic._PerformOnce = setmetatable({}, weakKeys)

---@class game.Event.Handler: instance
---@field subject any # subject of the event to be called on dispatch as first parameter
---@field name string # event name string to identify which event is this
---@field callback fun(...): any
---@field isRegistered boolean
---@field priority number
---@field nextHandler self?
---@field prevHandler self?
EventHandler = EventHandlerClass

---@param event game.Event
---@param eventName string
---@param callback fun(...): any
---@param subject any
function EventHandler:initialize(event, eventName, callback, subject)
	local _EventRegistry = EventHandlerClass._EventRegistry
	_EventRegistry[self] = event
	self.id = ('%p'):format(self)
	self.name = eventName
	self.callback = callback
	self.isRegistered = false
	self.subject = subject
	self.priority = 100
	self:register()
end

---Register a previously removed event handler (newly created handlers are already registered).
---@return self
function EventHandler:register()
	if self.isRegistered then
		return self
	end
	local _Event = EventHandlerClass._EventRegistry[self]
	self.nextHandler = _Event.handlers[self.name]
	if self.nextHandler then
		self.nextHandler.prevHandler = self
	end
	_Event.handlers[self.name] = self
	_Event.dirty[self.name] = true

	self.isRegistered = true

	return self
end

---Remove an event handler.
---@return self
function EventHandler:remove()
	if not self.isRegistered then
		return self
	end
	if self.prevHandler then
		self.prevHandler.nextHandler = self.nextHandler
	end
	if self.nextHandler then
		self.nextHandler.prevHandler = self.prevHandler
	end
	local _Event = EventHandlerClass._EventRegistry[self]
	if _Event.handlers[self.name] == self then
		_Event.handlers[self.name] = self.nextHandler
	end
	_Event.dirty[self.name] = true
	self.prevHandler, self.nextHandler = nil, nil
	self.isRegistered = false

	return self
end

---@class game.Event: instance
---@overload fun(event: string, ...)
---@field handlers table<string, game.Event.Handler>
---@field dirty table<string, boolean>
Event = EventClass

function Event:initialize()
	self.id = ('%p'):format(self)
	self.handlers, self.dirty = {}, {}
end

---@alias game.Event.argumentTable { name: string, subject: any, callback: fun(...): any }

---Adda new listener
---@overload fun(self: self, subject: any, eventName: string, callback: fun(...)): aNewHandler: game.Event.Handler
---@overload fun(self: self, eventName: string, callback: fun(...)): aNewHandler: game.Event.Handler
---@overload fun(self: self, subject: table, eventName: string): aNewHandler: game.Event.Handler
---@overload fun(self: self, argt: game.Event.argumentTable): aNewHandler: game.Event.Handler
function Event:on(...)
	local argc, argt, argz, eventHandler, eventName, callback, subject
	argc = select('#', ...)
	argz = type(select(1, ...)) == 'table'
	if argc == 1 then
		---input: [1] { name: string, subject: any, callback: fun(...): any }

		---@type game.Event.argumentTable
		argt = select(1, ...)
		eventName, callback, subject = argt.name, argt.callback, argt.subject
	elseif argc == 2 and argz then
		---input: [1] subject: table, [2] eventName: string
		---argz means first arg is table

		---@type table<string, function>
		subject = select(1, ...)
		---@type string
		eventName = select(2, ...)

		assert(subject[eventName], 'Method ' .. eventName .. 'under subject is not found.')
	elseif argc == 2 then
		---input: [1] eventName: string, [2] callback: fun(...)

		---@type string
		eventName = select(1, ...)
		---@type fun(subject: table, ...): any
		callback = select(2, ...)
	elseif argc == 3 then
		---input: [1] subject: any, [2] eventName: string, [3] callback: fun(...)
		---Ignorant subject. This handler has it own callback: does not look up the subject.

		---@type any
		subject = select(1, ...)
		---@type string
		eventName = select(2, ...)
		---@type fun(...): any
		callback = select(3, ...)
	else
		error('Failed to parse arguments.')
	end
	eventHandler = EventClass.Handler:new(self, eventName, callback, subject)
	return eventHandler
end

function Event:once(...)
	local handler = self:on(...)
	EventHandlerClass._PerformOnce[handler] = true
	return handler
end

---dispatch an event
---@param name string
---@param ... unknown
---@return game.Event.Handler|boolean # returns `false` if no handler is found
function Event:dispatch(name, ...)
	local handler = self.handlers[name]
	if not handler then return false end

	if self.dirty[name] then
		---@todo handler sorting goes here
		table.sort(handler)
		self.dirty[name] = false
	end

	while handler do
		local ret, callback, subject = nil, handler.callback, handler.subject

		if subject == nil then
			ret = callback(...)
		elseif type(subject) == 'table' then
			callback = callback or subject[name]
			ret = callback(subject, ...)
		else
			ret = callback(subject, ...)
		end

		if ret == false then
			return handler
		end
		if EventHandlerClass._PerformOnce[handler] then
			handler:remove()
			EventHandlerClass._PerformOnce[handler] = nil
			handler = self.handlers[name]
		else
			handler = handler.nextHandler
		end
	end

	return true
end

---@protected
function Event:__call(...)
	return self:dispatch(...)
end

return EventClass
