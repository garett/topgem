---@meta

---@class stateful: mixin
local stateful = {}

---@class stateful
stateful.static = {}

---This is a static member. It means available by the class.
---@type table<string, stateful>
stateful.static.states = {}

---This is a static method. It means to be executed by the class.
---
---@param name string
---@param super? table
---@return table newState
function stateful.static:addState(name, super) end


---Remove all stacked instance' states and set one.
---If `nil` is passed, revert to default (no state).
---@param name? string
---@param ... unknown # arguments to be passed to instance's `popAllStates` and next state's `enteredState`.
function stateful:gotoState(name, ...) end

---@param name string
---@param ... unknown # arguments to be passed to old state's `pausedState` and next state's `pushedState` and `enteredState`.
function stateful:pushState(name, ...) end

---@param name? string
---@param ... unknown # arguments to be passed to old state's `poppedState` and `exitedState` and next state's `continuedState`.
function stateful:popState(name, ...) end

---Remove all stacked instance' states. This calls `popState` of existing states until empty, basically.
---@param ... unknown # arguments to be passed to old state's `poppedState` and `exitedState` and next state's `continuedState`.
function stateful:popAllStates(...) end

---State's callback. Called once on instance's `gotoState` and `enteredState`. Think it like `love.load`, but for a state.
---@param ... unknown # passed arguments are from instance's `gotoState` and `enteredState`.
function stateful:enteredState(...) end

---State's callback. Called once on instance's `popState` (including `popAllStates`). Think it like `love.quit`, but for a state.
---@param ... unknown # passed arguments are from instance's `popState` (including `popAllStates`).
function stateful:exitedState(...) end

---State's callback. Called once on instance's `pushState`.
---@param ... unknown # passed arguments are from instance's `pushState`.
function stateful:pausedState(...) end

---State's callback. Called once on instance's `popState` (including `popAllStates`).
---@param ... unknown # passed arguments are from instance's `popState` (including `popAllStates`).
function stateful:continuedState(...) end

---State's callback. Called once on instance's `pushState`.
---@param ... unknown # passed arguments are from instance's `pushState`.
function stateful:pushedState(...) end

---State's callback. Called once on instance's `popState` (including `popAllStates`).
---@param ... unknown # passed arguments are from instance's `popState` (including `popAllStates`).
function stateful:poppedState(...) end
