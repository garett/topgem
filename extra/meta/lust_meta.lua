---@meta

---
---Taken from module's README.
---		https://github.com/bjornbytes/lust
---

---@class lust
local lust = {}

---Used to declare a group of tests.  `name` is a string used to describe the group, and `func` is a function containing all tests and `describe` blocks in the group.  Groups created using `describe` can be nested.
---@param name string
---@param func function
function lust.describe(name, func) end

---Used to declare a test, which consists of a set of assertions.  `name` is a string used to describe the test, and `func` is a function containing the assertions.
---@param name string
---@param func function
function lust.it(name, func) end

---Lust uses "expect style" assertions.  An assertion begins with `lust.expect(value)` and other modifiers can be chained after that.
---@param value any
---@return lust.action modifier
function lust.expect(value) end

---A sub-type of `luassert.internal` class.
---@class lust.action
local lust_action = {}

---The assertion.
---@type lust.action
lust_action.to = {}

---Negates the assertion.
---@type lust.action
lust_action.to_not = {}

---Performs an equality test using the `==` operator.  Fails if `x ~= y`.
---@overload fun(y): lust.action
---@type lust.action
lust_action.be = {}

---Fails only if `x` is `nil`.
---@param x any
---@return boolean success
---@return string whyFail
function lust_action.exist(x) end

---Performs a strict equality test, failing if `x` and `y` have different types or values.  Tables are tested by recursively ensuring that both tables contain the same set of keys and values.  Metatables are not taken into consideration.
---@param y any
function lust_action.equal(y) end

---Fails if `x` is `nil` or `false`.
function lust_action.truthy() end

---If `y` is a string, fails if `type(x)` is not equal to `y`.  If `y` is a table, walks up `x`'s metatable chain and fails if `y` is not encountered.
function lust_action.a(y) end

---If `x` is a table, ensures that at least one of its keys contains the value `y` using the `==` operator.  If `x` is not a table, this assertion fails.
function lust_action.have(y) end

---Ensures that the function `f` causes an error when it is run.
function lust_action.fail() end

---Fails if the string representation of `x` does not match the pattern `p`.
function lust_action.match(p) end

---Spies on a function and tracks the number of times it was called and the arguments it was called with.  There are 3 ways to specify arguments to this function:
---
--- - Specify `nil`.
--- - Specify a function.
--- - Specify a table and a name of a function in that table.
---
---The return value is a table that will contain one element for each call to the function. Each element of this table is a table containing the arguments passed to that particular invocation of the function.  The table can also be called as a function, in which case it will call the function it is spying on.  The third argument, `run`, is a function that will be called immediately upon creation of the spy.  
---
---@overload fun(function: function, run: function)
---@param table table
---@param key any
---@param run function
---@return table<any, any>
function lust.spy(table, key, run) end

---Set a function that is called before every test inside this `describe` block.  `fn` will be passed a single string containing the name of the test about to be run.
---@param fn function
function lust.before(fn) end

---Set a function that is called after every test inside this `describe` block.  `fn` will be passed a single string containing the name of the test that was finished.
---@param fn function
function lust.after(fn) end

---If Lua is embedded in an application and does not run in a console environment that understands ANSI color escapes, the library can be required as follows:
---
---    local lust = require('lust').nocolor()
---
---Warning: this method changes the internal variables. There is no way to restore expect reload the module.
---@return lust
function lust.nocolor() end