---@meta


---
---kikito middleclass class definition
---    THE FORK (and changelog): https://github.com/gphg/middleclass/tree/extra
---
---This class anotation does not scoop private or protected field for flexibility.
---

---@class middleclass
---@overload fun(className: string, super?: class, ...: unknown): aClass: class
local middleclass = {}

---@overload fun(name: string, super?: class, ...): (aClass: class|fun(self: class, ...): instance)
---@param className string
---@param super? class
---@param ... unknown
---@return class aClass
function middleclass.class(className, super, ...) end

---
---A Class' instance. Obtained by `aClass:new()` or `aClass()`
---
---@class instance<string,any>
local anInstance = {}

---
---Reference to instance's class.
---
---@return class aClass
function anInstance:class() end

---
---The class constructor.
---
---Normally you don't directly call this method, but by `Class:new` and it can only be called once.
---
---@param ... unknown
function anInstance:initialize(...) end

---
---Check if current instance a class of some class.
---
---@param other class
---@return boolean itIs
function anInstance:isInstanceOf(other) end


---
---[middleclass wiki: Reference](https://github.com/kikito/middleclass/wiki/Reference)
---
---@class class<string,any>
---@overload fun(): anInstance: instance
local aClass = {}

---
---Name of current class.
---
---@type string
aClass.name = ''

---
---Reference to this class' super or parent.
---
---@type class?
aClass.super = {}

---
---A class' table contains static methods and members.
---
---@type class
aClass.static = {}

---
---A static's table contains list of sub-classed of class in question.
---@type { [self]: true }
aClass.subclasses = {}

---
---A class method. Also available on its instance for reasons.
---
---@param ... unknown
function aClass:initialize(...) end

---
---A static method. Called before instance constructor.
---
---@return instance anInstance
function aClass:allocate() end

---
---A static method. Class instance constructor. Returns new instance.
---@param ... unknown
---@return instance anInstance
function aClass:new(...) end

---
---A static method. Create a class of this class in question.
---
---@overload fun(self: class, name: string): (aClass: class|fun(self: class, ...): instance)
---@param newClassName string
---@param ... unknown
---@return class aNewSubclass
function aClass:subclass(newClassName, ...) end

---
---A static method. Executed automatically as `Class:subclass` is called.
---
---@param aNewSubClass class
---@param ... unknown
function aClass:subclassed(aNewSubClass, ...) end

---
---A static method. Traverses the whole hierarchy of classes.
---
---@param super class
---@return boolean itIs
function aClass:isSubclassOf(super) end

---
---A static method. Include other mixin.
---
---@param ... mixin|table<string, function>
function aClass:include(...) end

---@protected
---@type metatable
aClass.__instanceDict = {}

---@protected
---@type table<string, function>
aClass.__declaredMethods = {}

---
---Mixins can be used for sharing methods between classes, without requiring them to inherit from the same father.
---
---[middleclass wiki: Mixins](https://github.com/kikito/middleclass/wiki/Mixins)
---
---@class mixin<string, function>
local aMixin = {}

---A static table contains static methods and members.
---
---@type mixin
---
aMixin.static = {}

---
---Call this function on class' constructor if a class includes this mixin. For example:
---
---    function myClass:initialize(...)
---        myMixin.initialize(self, ...)
---    end
---
---This method is not mentioned on middleclass' docs. I personaly suggest this.
---
---@param ... unknown
function aMixin:initialize(...) end

---
---A static method. Executed automatically as `OtherClass:include(thisMixin)` called.
---
---@param kclass class
function aMixin:included(kclass) end