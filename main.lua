if os.getenv("LOCAL_LUA_DEBUGGER_VSCODE") then require("lldebugger").start() end

---@class love
local love, require, setmetatable, assert, getenv =
	love, require, setmetatable, assert, os.getenv
local gameModulePath, sourceBaseDirMountPoint, filesystem, mountedPath
local __mount, __error, __layer, __pattern, __empty = 'mount', 'error', 2, '[\\/]+$', ''

gameModulePath = getenv('BASE_MODULE_PATH') or 'module'
sourceBaseDirMountPoint = getenv('SRC_BASEDIR_MOUNTPOINT') or '_'

-----------------------------------------------------------
-- Mount directories
-----------------------------------------------------------

filesystem, mountedPath = love.filesystem, {}
gameModulePath = gameModulePath:gsub(__pattern, __empty)
sourceBaseDirMountPoint = sourceBaseDirMountPoint:gsub(__pattern, __empty)

-- Only mount the very crucial directory contains modules. The rest should be done later.
if filesystem[__mount](filesystem.getSourceBaseDirectory(), sourceBaseDirMountPoint)
	and not filesystem.getInfo(gameModulePath, 'directory') then
	local fileData = assert(filesystem.newFileData(sourceBaseDirMountPoint .. '/resource1.dat'))
	assert(filesystem[__mount](fileData, gameModulePath), 'Unable to mount required directory: ' .. gameModulePath)
	mountedPath[gameModulePath] = fileData
end
_G.mounted = mountedPath

-----------------------------------------------------------
-- Module pre-loading
--------------------------------------------------------------

---Some libraries (trusted ones) requires to modify the global.
require 'socket'

-- lock the global table (not really locking, just raise error).
setmetatable(_G, {
	__index = function(t) t[__error]('referenced an undefined variable', __layer) end,
	__newindex = function(t) t[__error]('new global variables disabled', __layer) end,
})

---Load the rest of libraries
require(gameModulePath) {
	---Preload modules: order matters.
	preload = {
		'middleclass',
		'stateful',
		'ww',
		'game',
		'nest',
	}
}

--[[ commented out because initialization requires window to be present.
require("nest"):init({
        -- required. available options are: 'ctr' (3DS) or 'hac' (Switch)
        mode = getenv('CONSOLE_PORTLIB') or 'hac',
})
]]
--

-----------------------------------------------------------
-- Love setups
--------------------------------------------------------------

---@type love.load
love.load = require('game') {
	overwrite = true,
}
